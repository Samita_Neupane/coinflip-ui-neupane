import rp from "request-promise";

export function getCoinflip(id) {
  return rp({
    method: "GET",
    uri: "https://sneupane-coinflip.herokuapp.com/coinflip/" + id,
    json: true
  });
}


