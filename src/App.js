import React, { Component } from "react";
import { getCoinflip } from "./coinflipService";
import ChildComponent from "./ChildComponent";
import { Input, Button, Alert } from "reactstrap";

import "./App.css";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outcome: " ",
      image: " ",
      outcomePercentage: " ",
      individualFlips: [],
      coinflipLoaded: false
    };
    this.setId = this.setId.bind(this);
    this.getCoinflipFromAPI = this.getCoinflipFromAPI.bind(this);
  }

  getCoinflipFromAPI() {
    getCoinflip(this.state.id).then(coinflip => {
      this.setState({
        outcome: coinflip.outcome,
        image: coinflip.coinImage,
        outcomePercentage: coinflip.outcomePercentage,
        individualFlips: coinflip.individualFlips,
        coinflipLoaded: true
      });
    });
  }

  setId(event) {
    this.setState({ id: event.target.value });
  }

  render() {
    console.log(this.state.coinflipLoaded);
    let coinflipOutput = this.state.coinflipLoaded ? (
      <div>
        <div>outcome: {this.state.outcome}</div>
        <div>
          image: <img src={this.state.image} />
          <div>outcomePercentage:{this.state.outcomePercentage}</div>
        </div>
      </div>
    ) : (
      ""
    );

    let alert =
      this.state.id < 1 || this.state.id > 29 || this.state.id % 2 == 0 ? (
        <Alert className="alert" color="danger">
          PLEASE ENTER A VALID NUMBER
        </Alert>
      ) : (
        ""
      );
    return (
      <div className="App">
        <header className="App-header">
          Welcome To The CoinFlip UI
          <img
            src="https://art.ngfiles.com/images/580000/580680_wondermeow_coin-flip.gif?f1515950768"
            alt="Flip The Coin"
            class="center"
          />
          <Input
            className="input"
            onChange={this.setId}
            placeholder="FlipNumbers"
            type="number"
            min="1"
            max="29"
          />
          <Button className="button" onClick={this.getCoinflipFromAPI}>
            Flip The Coin
          </Button>
          {alert}
          {coinflipOutput}
          <ChildComponent individualFlips={this.state.individualFlips} />
        </header>
      </div>
    );
  }
}
