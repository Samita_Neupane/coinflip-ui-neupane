import React, { Component } from "react";

export default class ChildComponent extends Component {
  constructor(props) {
    super(props); //constructor & super calls w/ props argument are required for props to work
  }

  render() {
    let arrFlips = [];
    this.props.individualFlips.forEach(flip => {
      console.log(flip);
      arrFlips.push(
        <div>
          side:{flip.side}
          <img src={flip.image} />
        </div>
      );
    });
    return (
      <div>
        <div>OUTCOMES</div>
        {arrFlips}
      </div>
    );
  }
}
